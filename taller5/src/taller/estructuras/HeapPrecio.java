package taller.estructuras;

public class HeapPrecio <T extends Comparable<T>> implements IHeap {
	
	private int tamMax;
	private int tamActual;
	private T[] pq;
	
	public HeapPrecio(int pTamMax){
		tamMax = pTamMax;
		pq = (T[]) new Comparable[tamMax + 1];
		tamActual = 0;
	}

	@Override
	public void add(Object elemento) {
		// TODO Auto-generated method stub
		if(isEmpty()){
			pq[1] = (T) elemento;
		}
		else if(tamActual + 5 >= tamMax){
			T[] nuevaLista = (T[]) new Comparable[tamMax + 20];
			for(int i = 1; i <pq.length; i++){
				nuevaLista[i] = pq[i];
			}
			pq = nuevaLista;
			pq[tamActual + 1] = (T) elemento;
		}
		else{
			pq[tamActual + 1] = (T) elemento;
		}
		
		tamActual ++;
	}

	@Override
	public Object peek() {
		// TODO Auto-generated method stub
		if(isEmpty()){
			return null;
		}
		else{
			Object aRetornar = pq[1];
			return aRetornar;
		}
	}

	@Override
	public Object poll() {
		// TODO Auto-generated method stub
		if(isEmpty()){
			return null;
		}
		else{
			Object aRetornar = pq[1];
			pq[1] = null;
			exch(1,tamActual);
			tamActual--;
			siftDown();
			return aRetornar;
			
		}
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return tamActual;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return tamActual == 0;
	}

	@Override
	public void siftUp() {
		// TODO Auto-generated method stub
		int k = tamActual;
		while(k > 1 && less(k/2, k)){
			exch(k/2, k);
			k = k/2;
		}
	}

	@Override
	public void siftDown() {
		// TODO Auto-generated method stub
		int k = 1;
		while(2 * k <= tamActual){
			int j = 2*k;
			if(j < tamActual  && less(j, j+1)) j++;
			if( !less(k, j)) break;
			exch(k,j);
			k = j;
		}
		
	}
	
	public void exch(int i, int j){
		T temp = pq[i];
		pq[i] = pq[j];
		pq[j] = temp;
	}
	
	private boolean less(int i,int j){
		return pq[i].compareTo(pq[j]) < 0;
	}
	
	public Object[] clonPq(){
		Object[] lista = new Object[pq.length];
		for(int i = 0; i < pq.length; i++){
			lista[i] = pq[i];
		}
		return lista;
	}

	

	


}
