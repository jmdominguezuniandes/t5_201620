package taller.mundo;

public class Pedido implements Comparable
{

	// ----------------------------------
	// Atributos
	// ----------------------------------

	/**
	 * Precio del pedido
	 */
	private double precio;
	
	/**
	 * Autor del pedido
	 */
	private String autorPedido;
	
	/**
	 * Cercania del pedido
	 */
	private int cercania;
	
	/**
	 * Estado del pedido
	 */
	private String estado;
	
	// ----------------------------------
	// Constructor
	// ----------------------------------
	
	/**
	 * Constructor del pedido
	 * TODO Defina el constructor de la clase
	 */
	public Pedido(double pPrecio, String pAutorPedido, int pCercania, String pEstado){
		precio = pPrecio;
		autorPedido = pAutorPedido;
		cercania = pCercania;
		estado = pEstado;
	}
	
	// ----------------------------------
	// Métodos
	// ----------------------------------
	
	/**
	 * Getter del precio del pedido
	 */
	public double getPrecio()
	{
		return precio;
	}
	
	/**
	 * Getter del autor del pedido
	 */
	public String getAutorPedido()
	{
		return autorPedido;
	}
	
	/**
	 * Getter de la cercania del pedido
	 */
	public int getCercania() {
		return cercania;
	}
	
	public void cambiarEstado(String pNuevoEstado){
		estado = pNuevoEstado;
	}
	
	// TODO 
	

	@Override
	public int compareTo(Object arg0) {
		// TODO Auto-generated method stub
		Pedido pPedido = (Pedido) arg0;
		if(estado.equals("porAtender")){
			if(precio <= pPedido.getPrecio()){
				return -1;
			}
			else{
				return 1;
			}
		}
		else{
			if(cercania < pPedido.getCercania()){
				return -1;
			}
			else{
				return 1;
			}
		}
	}
}
