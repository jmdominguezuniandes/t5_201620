package taller.mundo;

import taller.estructuras.HeapPrecio;
import taller.estructuras.Heapcercania;


public class Pizzeria
{	
	// ----------------------------------
    // Constantes
    // ----------------------------------
	
	/**
	 * Constante que define la accion de recibir un pedido
	 */
	public final static String RECIBIR_PEDIDO = "RECIBIR";
	/**
	 * Constante que define la accion de atender un pedido
	 */
	public final static String ATENDER_PEDIDO = "ATENDER";
	/**
	 * Constante que define la accion de despachar un pedido
	 */
	public final static String DESPACHAR_PEDIDO = "DESPACHAR";
	/**
	 * Constante que define la accion de finalizar la secuencia de acciones
	 */
	public final static String FIN = "FIN";
	
	// ----------------------------------
    // Atributos
    // ----------------------------------
	
	/**
	 * Heap que almacena los pedidos recibidos
	 */
	// TODO 
	private HeapPrecio pedidosRecibidos;
	/**
	 * Getter de pedidos recibidos
	 */
	HeapPrecio darHeapPedidosRecibidos(){
		return pedidosRecibidos;
	}
	// TODO 
 	/** 
	 * Heap de elementos por despachar
	 */
	private Heapcercania pedidosPorDespachar;
	// TODO 
	/**
	 * Getter de elementos por despachar
	 */
	Heapcercania darPedidosPorDespachar(){
		return pedidosPorDespachar;
	}
	// TODO 
	
	// ----------------------------------
    // Constructor
    // ----------------------------------

	/**
	 * Constructor de la case Pizzeria
	 */
	public Pizzeria()
	{
		// TODO 
		pedidosRecibidos = new HeapPrecio(10);
		pedidosPorDespachar = new Heapcercania(10);
	}
	
	// ----------------------------------
    // Métodos
    // ----------------------------------
	
	/**
	 * Agrega un pedido a la cola de prioridad de pedidos recibidos
	 * @param nombreAutor nombre del autor del pedido
	 * @param precio precio del pedido 
	 * @param cercania cercania del autor del pedido 
	 */
	public void agregarPedido(String nombreAutor, double precio, int cercania)
	{
		// TODO 
		Pedido p = new Pedido(precio, nombreAutor, cercania, "porAtender");
		Object nuevo = (Object) p;
		pedidosRecibidos.add(nuevo);
	}
	
	// Atender al pedido más importante de la cola
	
	/**
	 * Retorna el proximo pedido en la cola de prioridad o null si no existe.
	 * @return p El pedido proximo en la cola de prioridad
	 */
	public Pedido atenderPedido()
	{
		// TODO 
		if(pedidosRecibidos.isEmpty()){
			return null;

		}
		else{
			Pedido porAtender = (Pedido) pedidosRecibidos.poll();
			porAtender.cambiarEstado("porDespachar");
			pedidosPorDespachar.add(porAtender);
			return porAtender;
		}
		
	}

	/**
	 * Despacha al pedido proximo a ser despachado. 
	 * @return Pedido proximo pedido a despachar
	 */
	public Pedido despacharPedido()
	{
		// TODO 
	    if(pedidosPorDespachar.isEmpty()){
	    	return null;
	    }
	    else{
	    	Pedido porDespachar = (Pedido) pedidosPorDespachar.poll();
	    	return porDespachar;
	    }
	}
	
	 /**
     * Retorna la cola de prioridad de pedidos recibidos como un arreglo. 
     * @return arreglo de pedidos recibidos manteniendo el orden de la cola de prioridad.
     */
     public Pedido [] pedidosRecibidosList()
     {
        // TODO 
    	 if(pedidosRecibidos.size() == 0){
    		 return new Pedido[0];
    	 }
    	 
    	 
    	 
    	 Object lista[] = pedidosRecibidos.clonPq();
    	 Pedido p[] = new Pedido[pedidosRecibidos.size()];
    	 int i = 1;
    	 int j = 0;
    	 while(i < lista.length){
    		 if(lista[i] == null){
    			 
    		 }
    		 else{
    		 p[j] = (Pedido) lista[i];
    		 j++;
    		 }
    		 i++;
    	 }
    	 
    	 insertion(p);
    	 
    	 
    	 return p;
        
        
     }
     
      /**
       * Retorna la cola de prioridad de despachos como un arreglo. 
       * @return arreglo de despachos manteniendo el orden de la cola de prioridad.
       */
     public Pedido [] colaDespachosList()
     {
         // TODO 
    	 if(pedidosPorDespachar.size() == 0){
    		 return new Pedido[0];
    	 }
    	 
    	 Object lista[] = pedidosPorDespachar.clonPq();
    	 Pedido p[] = new Pedido[pedidosPorDespachar.size()];
    	 int i = 1;
    	 int j = 0;
    	 while(i < lista.length){
    		 if(lista[i] == null){
    			 
    		 }
    		 else{
    		 p[j] = (Pedido) lista[i];
    		 j++;
    		 }
    		 i++;
    	 }
    	 
    	 return p;
     }
     
     public void insertion(Pedido[] array ){
    	
    	 for(int i = 0; i< array.length; i++){
    		 for(int j = i; j > 0 && !(array[j].compareTo(array[j-1]) < 0) ; j--){
    			 Pedido temp = array[j];
    			 array[j] = array[j -1];
    			 array[j- 1] = temp;
    		 }
    	 }
     }
     
     public void insertion2(Pedido[] array){
    	 for(int i = 0; i< array.length; i++){
    		 for(int j = i; j > 0 && (array[j].compareTo(array[j-1]) < 0) ; j--){
    			 Pedido temp = array[j];
    			 array[j] = array[j -1];
    			 array[j- 1] = temp;
    		 }
    	 }
     }
}
